<!-- omit in toc -->
# Contributing to FALL3D
First off, thanks for taking the time to contribute!

All types of contributions are encouraged and valued.
See the [Table of Contents](#table-of-contents) for different ways
to help and details about how this project handles them.
Please make sure to read the relevant section before making your contribution.
It will make it a lot easier for us maintainers and smooth out the
experience for all involved. The community looks forward to your contributions.

> And if you like the project, but just don't have time to contribute, 
> that's fine. There are other easy ways to support the project and show
> your appreciation, which we would also be very happy about:
> - Star the project
> - Tweet about it
> - Refer this project in your project's readme
> - Mention the project at local meetups and tell your friends/colleagues

<!-- omit in toc -->
## Table of Contents

* [Code of Conduct](#code-of-conduct)
* [I Have a Question](#i-have-a-question)
* [I Want To Contribute](#i-want-to-contribute)
  + [Reporting Bugs](#reporting-bugs)
    - [Before Submitting a Bug Report](#before-submitting-a-bug-report)
    - [How Do I Submit a Good Bug Report?](#how-do-i-submit-a-good-bug-report)
  + [Suggesting Enhancements](#suggesting-enhancements)
    - [Before Submitting an Enhancement](#before-submitting-an-enhancement)
    - [How Do I Submit a Good Enhancement Suggestion?](#how-do-i-submit-a-good-enhancement-suggestion)
  + [Your First Code Contribution](#your-first-code-contribution)
    - [Make a fork of FALL3D](#make-a-fork-of-fall3d)
    - [Clone your fork](#clone-your-fork)
    - [Keeping your fork up to date](#keeping-your-fork-up-to-date)
    - [Making changes](#making-changes)
    - [Open a merge request](#open-a-merge-request)
  + [Improving The Documentation](#improving-the-documentation)
* [Styleguides](#styleguides)
  + [Commit Messages](#commit-messages)
  + [Branching standards & conventions](#branching-standards-conventions)
* [Join The Project Team](#join-the-project-team)
* [Attribution](#attribution)

## Code of Conduct
This project and everyone participating in it is governed by the
[FALL3D Code of Conduct](https://gitlab.com/fall3d-suite/fall3d/-/blob/master/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. 
Please report unacceptable behavior to <lmingari@geo3bcn.csic.es>.

## I Have a Question
> If you want to ask a question, we assume that you have read the available 
[Documentation](https://fall3d-suite.gitlab.io/fall3d).

Before you ask a question, it is best to search for existing 
[Issues](https://gitlab.com/fall3d-suite/fall3d/issues) that might help you.
In case you have found a suitable issue and still need clarification, 
you can write your question in this issue. It is also advisable to search
the internet for answers first.

If you then still feel the need to ask a question and need clarification, 
we recommend the following:

- Open an [Issue](https://gitlab.com/fall3d-suite/fall3d/issues/new).
- Provide as much context as you can about what you're running into.
- Provide project and platform versions, depending on what seems relevant.

We will then take care of the issue as soon as possible.

## I Want To Contribute

> ### Legal Notice <!-- omit in toc -->
> When contributing to this project, you must agree that you have
> authored 100% of the content, that you have the necessary rights
> to the content and that the content you contribute may be provided
> under the project license.

### Reporting Bugs

<!-- omit in toc -->
#### Before Submitting a Bug Report
A good bug report shouldn't leave others needing to chase you up
for more information. Therefore, we ask you to investigate carefully,
collect information and describe the issue in detail in your report.
Please complete the following steps in advance to help us fix any
potential bug as fast as possible.

- Make sure that you are using the latest version.
- Determine if your bug is really a bug and not an error on your side
  e.g. using incompatible environment components/versions (Make sure
  that you have read the [documentation](https://fall3d-suite.gitlab.io/fall3d).
  If you are looking for support, you might want to check
  [this section](#i-have-a-question)).
- To see if other users have experienced (and potentially already solved)
  the same issue you are having, check if there is not already a bug report
  existing for your bug or error in the
  [bug tracker](https://gitlab.com/fall3d-suite/fall3dissues?q=label%3Abug).
- Also make sure to search the internet (including Stack Overflow) to see if
  users outside of the GitHub community have discussed the issue.
- Collect information about the bug:
  - Stack trace (Traceback)
  - OS, Platform and Version (Windows, Linux, macOS, x86, ARM)
  - Version of the interpreter, compiler, SDK, runtime environment,
    package manager, depending on what seems relevant.
  - Possibly your input and the output
  - Can you reliably reproduce the issue? And can you also reproduce it
    with older versions?

<!-- omit in toc -->
#### How Do I Submit a Good Bug Report?
> You must never report security related issues, vulnerabilities or bugs
> including sensitive information to the issue tracker, or elsewhere in public.
> Instead sensitive bugs must be sent by email to <lmingari@geo3bcn.csic.es>.

We use GitLab issues to track bugs and errors. If you run into an issue
with the project:
- Open an [Issue](https://gitlab.com/fall3d-suite/fall3d/issues/new).
  (Since we can't be sure at this point whether it is a bug or not,
  we ask you not to talk about a bug yet and not to label the issue.)
- Explain the behavior you would expect and the actual behavior.
- Please provide as much context as possible and describe the *reproduction steps*
  that someone else can follow to recreate the issue on their own.
  This usually includes your code. For good bug reports you should
  isolate the problem and create a reduced test case.
- Provide the information you collected in the previous section.

Once it's filed:

- The project team will label the issue accordingly.
- A team member will try to reproduce the issue with your provided steps.
  If there are no reproduction steps or no obvious way to reproduce the issue,
  the team will ask you for those steps and mark the issue as `needs-repro`.
  Bugs with the `needs-repro` tag will not be addressed until they are reproduced.
- If the team is able to reproduce the issue, it will be marked `needs-fix`,
  as well as possibly other tags (such as `critical`), and the issue will be
  left to be [implemented by someone](#your-first-code-contribution).

### Suggesting Enhancements
This section guides you through submitting an enhancement suggestion for FALL3D,
**including completely new features and minor improvements to existing functionality**.
Following these guidelines will help maintainers and the community to understand
your suggestion and find related suggestions.

<!-- omit in toc -->
#### Before Submitting an Enhancement
- Make sure that you are using the latest version.
- Read the [documentation](https://fall3d-suite.gitlab.io/fall3d) carefully and
  find out if the functionality is already covered, maybe by an individual configuration.
- Perform a [search](https://gitlab.com/fall3d-suite/fall3d/issues) to see if
  the enhancement has already been suggested. If it has, add a comment to the
  existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project.
  It's up to you to make a strong case to convince the project's developers of
  the merits of this feature. Keep in mind that we want features that will be
  useful to the majority of our users and not just a small subset. If you're
  just targeting a minority of users, consider writing an add-on/plugin library.

<!-- omit in toc -->
#### How Do I Submit a Good Enhancement Suggestion?
Enhancement suggestions are tracked as [GitLab issues](https://gitlab.com/fall3d-suite/fall3d/issues).

- Use a **clear and descriptive title** for the issue to identify the suggestion.
- Provide a **step-by-step description of the suggested enhancement** in as many 
  details as possible.
- **Describe the current behavior** and **explain which behavior you expected to see instead** 
  and why. At this point you can also tell which alternatives do not work for you.
- **Explain why this enhancement would be useful** to most FALL3D users. 
  You may also want to point out the other projects that solved it better 
  and which could serve as inspiration.

### Your First Code Contribution
All contributions to FALL3D code must be made using the fork and merge request 
workflow, which must then be reviewed by one of the project maintainers, 
following the next development workflow.

#### Make a fork of FALL3D
1. Go to the [FALL3D repository](https://gitlab.com/fall3d-suite/fall3d) home page
2. Click on the *Fork* button
3. Select the namespace that you want to create the fork in, 
   this will usually be your personal namespace

#### Clone your fork
Clone your fork with the command:
```bash
git clone git@gitlab.com:<namespace>/fall3d.git
```
#### Keeping your fork up to date
Link your clone to the main (`upstream`) repository so that you can `fetch` changes,
`merge` them with your clone, and `push` them to your fork. 

1. Link your fork to the main repository:
   ```bash
   cd fall3d
   git remote add upstream git@gitlab.com:fall3d-suite/fall3d.git
   ```
   You need only do this step once.
2. Fetch new changes from the `upstream` repository, merge them with your master branch,
   and push them to your fork:
   ```bash
   git checkout master
   git fetch upstream
   git merge --ff-only upstream/master
   git push
   ```
3. You can see which remotes are configured using
   ```bash
   git remote -v
   ```
   If you have followed the instructions thus far, you should see four lines. 
   Lines one and two begin with `origin` and reference your fork with
   both `fetch` and `push` methods. Lines three and four begin with `upstream`
   and refer to the main repository with both `fetch` and `push` methods.

#### Making changes
All changes should be developed on a feature branch `feature/my_name` or
a hotfix branch `hotfix/my_name` according to the following workflow.
See Section [Branching standards & conventions](#branching-standards-conventions)
for further details and branching conventions.

1. Create a new branch configured to track the `master` branch of the 
   `upstream` repository:
   ```bash
   git checkout -b feature/my_name upstream/master
   ```
   This command creates the new branch `feature/my_name`, 
   sets up tracking the `upstream` repository, 
   and checks out the new branch. 
2. Develop the changes you would like to introduce, using `git commit` 
   to finalise a specific change. Ideally commit small units of change 
   often, rather than creating one large commit at the end, this will 
   simplify review and make modifying any changes easier.
3. Push your changes to the remote copy of your fork on `origin`.
   The first `push` of any new feature branch will require the 
   `-u/--set-upstream` option to `push` to create a link between 
   your new branch and the `origin` remote:
   ```bash
   git push --set-upstream origin feature/my_name
   ```
   Subsequent pushes can be made with 
   ```bash
   git push origin feature/my_name
   ```  
4. Keep your feature branch up to date with the `upstream` repository by doing 
   ```bash
   git checkout feature/my_name
   git fetch upstream
   git rebase upstream/master
   git push --force-with-lease origin feature/my_name
   ```
   This works if you created your branch with the `checkout` command above. 
   If you forgot to add the `upstream/master` starting point, then you will 
   need to dig deeper into git commands to get changes and merge them into 
   your feature branch. 

   If there are conflicts between `upstream` changes and your changes, you 
   will need to resolve them before pushing everything to your fork.

   > **Prefer git rebase!** `rebase` works similar with `merge` but the 
   > result is much cleaner in git history and using rebase is encouraged.
   > However, rebase literally rewrites Git commit history and can be a very 
   > destructive operation. 
   >
   > **Important rule!** Don't rebase a branch that multiple people have 
   > access to! Only rebase branches in your fork.

#### Open a merge request
When you feel that your work is finished, you should create a merge request 
to propose that your changes be merged into the main (`upstream`) repository.
After you have pushed your new feature branch to `origin`, you should find a 
new button on the FALL3D repository home page inviting you to create a merge 
request out of your newly pushed branch or you can initiate a merge request by 
going to the *Merge Requests* tab on your fork website.

It is recommended that you check the box to *Remove source branch when merge 
request is accepted*; this will result in the branch being automatically 
removed from your fork when the merge request is accepted. 

Once the request has been opened, one of the maintainers will assign someone 
to review the change. There may be suggestions and/or discussion with the
reviewer. These interactions are intended to make the resulting changes better. 
The reviewer will merge your request.

Once the changes are merged into the upstream repository, 
you should remove the development branch from your clone using 
```
git branch -d feature/my_name
```
A feature branch should *not* be repurposed for further development as this 
can result in problems merging upstream changes. 

### Improving The Documentation
Updating, improving and correcting the documentation is also encouraged. 
The documentation can be found in the `Manual` folder. Just edit or correct
the markdown files there. Create a merge request when you work is finished.

## Styleguides
### Commit Messages
Commit messages should be clear, identifying which code was changed, and why.
Common practice is to use a short summary line (<50 characters), followed by 
a blank line, then more information in longer lines.

### Branching standards & conventions
The Git branching model uses a long-lived `master` branch and multiple 
`feature`/`hotfix` short-lived branches with a few commits. Eventually, 
a `release` branch could also exist.

The `master` branch is the main branch where the source code of `HEAD` 
always reflects a state with the latest delivered development changes 
for the next release.
Feature branches are used when developing a new feature or enhancement 
which has the potential of a development lifespan longer than a single 
deployment. 
Hotfix branches will be created when there is a bug or an 
issue and developers need to quickly fix it.

| Instance | Branch    | Creation                      | Update |
|----------|-----------|-------------------------------|--------|
| Working  | master    |                               | Accepts merges from `feature`/`hotfix` |
| Releases | release/* | Branch off `master`           | Accepts merges/commits from `hotfix`   |
| Features | feature/* | Branch off `master`           |        |
| Hotfixes | hotfix/*  | Branch off `master`/`release` |        |

> Do not make changes on your `master` branch. 
> Please use `feature` branches for new features and `hotfix` for quick 
  fixes of bugs following the name convection specified in the table above. 
  For example, `feature/add_option` or `hotfix/bug_dateformat`.

## Join The Project Team
If you are interested in joining our team, please contact us.

<!-- omit in toc -->
## Attribution
This guide is based on the **contributing-gen**. 
[Make your own](https://github.com/bttger/contributing-gen)!